import java.util.Scanner;

public class Variables {
    public static void main(String[] args) {
        int age = 1;
        String name = "Neko Program";
        String username = "nekoprogram";
        String aFood = "";

        Scanner scanner = new Scanner(System.in);

        System.out.println("My name is " + name + " and my username is " + username);
        System.out.println("My Current age is " + age + " nyan_years :3");

        boolean isCat = true;

        if (isCat) {
            System.out.println(name + " is a cat.");
        } else {
            System.out.println(name + " is not a cat.");
        }

        System.out.println("What's your favourite food? ");

        aFood = scanner.nextLine();

        System.out.println("Your favourite food is  " + aFood + ", " + name);

        scanner.close();

        
        
    }
}
