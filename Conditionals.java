public class Conditionals {
    public static void main(String[] args) {
        
        int age = 1;
        String name = "Neko Program";
        String username = "nekoprogram";

        System.out.println("My name is " + name + " and my username is " + username);
        System.out.println("My Current age is "  + age + " nyan_years :3");


        if (age < 4)
        {
            System.out.println("You are a kitten :3");
        }
        else 
        {
            System.out.println("You are a BIG CAT >.<");
        }

        boolean isActive = false;
        String account_string_status = "deactivated";

        if(isActive) {

            account_string_status = "active";
        }

        System.out.println("Your twitter account is " + account_string_status);
    }
}
